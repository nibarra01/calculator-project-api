package com.calculator.app.crtl;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "calc")
@CrossOrigin
public class calctroller {

    @GetMapping("/add")
    public double add(@RequestParam(name="x") double x, @RequestParam(name="y") double y){
        return x + y;
    }

    @GetMapping("/sub")
    public double sub(@RequestParam(name="x") double x, @RequestParam(name="y") double y){
        return x - y;
    }

    @GetMapping("/mul")
    public double multi(@RequestParam(name="x") double x, @RequestParam(name="y") double y){
        return x * y;
    }

    @GetMapping("/div")
    public String div(@RequestParam(name="x") double x, @RequestParam(name="y") double y){

        double a = x / y;
        double b = x % y;
        String A = null;
        String B = null;

        if (a % 1 == 0){
             A = Long.toString((long) a);
        } else {
             A = Double.toString(a);
        }


        if (b % 1 == 0){
             B = Long.toString((long) b);
        } else {
             B = Double.toString(b);
        }

        if (b != 0){
            return A + " with a remainder of " + B;
        } else {
            return A;
        }
    }

    @GetMapping("/pow")
    public double pow(@RequestParam(name="x") double x, @RequestParam(name="y") double y){
        return Math.pow(x, y);
    }

    @GetMapping("/per")
    public double per(@RequestParam(name="x") double x, @RequestParam(name="y") double y){

        return (x / 100) * y;
    }
}
